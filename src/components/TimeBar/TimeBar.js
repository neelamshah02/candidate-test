import React, { Component } from "react";
import style from "./TimeBar.css";
class TimeBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startTime: this.props.prevTime,
      nextTime: this.props.prevTime
    };
    this.getTimeIntervalComponent = this.getTimeIntervalComponent.bind(this);
  }

  getTimeIntervalComponent() {
    let TimeBarDiv = [];
    let timelength = 0;
    let prevTime = new Date(1482224400000);
    let nextTime = new Date(1482224400000);

    while (timelength < 5000) {
      nextTime.setMinutes(nextTime.getMinutes() + 30);
      timelength = timelength + (30 * 60 * 1000 * 5000) / (24 * 3600 * 1000);

      TimeBarDiv.push(new Date(nextTime));
    }

    return TimeBarDiv;
  }

  render() {
    return (
      <div className="time-bar">
        {this.getTimeIntervalComponent().map((time, key) => {
          return (
            <div
              className="time-details-div"
              style={{ marginLeft: 5000 / 48 - 31 }}
              key={key}
            >
              {time.getHours() + ":" + time.getMinutes()}
            </div>
          );
        })}
      </div>
    );
  }
}

export default TimeBar;
