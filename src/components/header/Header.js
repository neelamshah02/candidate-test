import React, { Component } from "react";
import style from "./Header.css";
import { FaBeer, FaAdn, FaUserAlt, FaSearch } from "react-icons/fa";

class Header extends Component {
  render() {
    return (
      <div id="header-wrapper">
        <div id="user_icon" className="header-icon-class">
          <FaUserAlt />
        </div>
        <div id="logo" className="header-icon-class">
         <img  className="nm-logo" src="../../../mockups/nm.png"></img>
        </div>
        <div id="search" className="header-icon-class">
          <FaSearch />
        </div>
      </div>
    );
  }
}

export default Header;
