import React, { Component } from "react";
import { FaStar } from "react-icons/fa";
import style from "./Datebar.css";

const WeekDay = ["Mon", "Tue", "Wed", "Thr", "Fri", "Sat", "Sun"];

class DateBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datetimeline: [
        "02/09/2019",
        "02/10/2019",
        "02/11/2019",
        "02/12/2019",
        "02/13/2019",
        "02/14/2019",
        "02/15/2019",
        "02/16/2019"
      ]
    };
  }
  render() {
    return (
      <div id="datebar-wrapper">
        <div id="datebar-logo">
          <FaStar />
        </div>
        <div id="date-container">
          {this.state.datetimeline.map((date, key) => {
            return (
              <div className="datebar-date-div" key={key}>
                <div className="datebar-day">
                  {WeekDay[new Date(date).getDay()]}
                </div>
                <div className="datebar-date">
                  {new Date(date).getDate() +
                    "." +
                    (new Date(date).getMonth() + 1) +
                    "."}
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default DateBar;
