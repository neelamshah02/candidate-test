import React, { Component } from "react";
import style from "./Nowbutton.css";

class NowButtom extends Component {
  constructor(props) {
    super(props);
    this.scrollToCurrent = this.scrollToCurrent.bind(this);
  }
  scrollToCurrent() {
    var leftElemMargin = document.getElementById("current-indicator").style
      .marginLeft;
    document.getElementById("current-indicator").scrollIntoView({
      behavior: "smooth"
    });
  }

  render() {
    return (
      <div id="now-button" onClick={this.scrollToCurrent}>
        Now
      </div>
    );
  }
}

export default NowButtom;
