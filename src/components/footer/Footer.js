import React, { Component } from "react";
import style from "./Footer.css";
import {
  FaHome,
  FaAdn,
  FaUserAlt,
  FaSearch,
  FaBookReader,
  FaList,
  FaRegPlayCircle,
  FaUndoAlt
} from "react-icons/fa";

class Footer extends Component {
  render() {
    return (
      <div id="footer-wrapper">
        <div id="home" className="footer-icon-class">
          <FaHome />
        </div>
        <div id="shows" className="footer-icon-class">
          <FaRegPlayCircle />
        </div>
        <div id="list" className="footer-icon-class">
          <FaList />
        </div>
        <div id="replay" className="footer-icon-class">
          <FaUndoAlt />
        </div>
        <div id="read" className="footer-icon-class">
          <FaBookReader />
        </div>
      </div>
    );
  }
}

export default Footer;
