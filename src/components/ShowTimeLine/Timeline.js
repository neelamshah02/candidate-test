import React, { Component } from "react";
import style from "./Timeline.css";
import NowButton from "../NowButton/Nowbutton";
import TimeBar from "../TimeBar/TimeBar";

class TimeLine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      channel_info: [],
      images : [
         "https://images.radiotimes.com/remote/images.atlas.metabroadcast.com/pressassociation.com/channels/p220121121742.png?quality=60&mode=fit&width=60&height=45&404=tv",
         "https://images.radiotimes.com/remote/images.atlas.metabroadcast.com/pressassociation.com/channels/p22016021154.png?quality=60&mode=fit&width=60&height=45&404=tv",
         "https://images.radiotimes.com/remote/images.atlas.metabroadcast.com/pressassociation.com/channels/p22012112152.png?quality=60&mode=fit&width=60&height=45&404=tv",         
         "https://images.radiotimes.com/remote/images.atlas.metabroadcast.com/pressassociation.com/channels/p22016101374.png?quality=60&mode=fit&width=60&height=45&404=tv",
        "https://images.radiotimes.com/remote/images.atlas.metabroadcast.com/pressassociation.com/channels/p220121121742.png?quality=60&mode=fit&width=60&height=45&404=tv",
         "https://images.radiotimes.com/remote/images.atlas.metabroadcast.com/pressassociation.com/channels/p220121121713.png?quality=60&mode=fit&width=60&height=45&404=tv",
         "https://images.radiotimes.com/remote/images.atlas.metabroadcast.com/pressassociation.com/channels/p22019020549.png?quality=60&mode=fit&width=60&height=45&404=tv",
         "https://images.radiotimes.com/remote/images.atlas.metabroadcast.com/pressassociation.com/channels/p220150813451.png?quality=60&mode=fit&width=60&height=45&404=tv",
         "https://images.radiotimes.com/remote/images.atlas.metabroadcast.com/pressassociation.com/channels/p2201805031798.png?quality=60&mode=fit&width=60&height=45&404=tv"
      ]
    };
  }
  componentDidMount() {
    fetch("http://localhost:1337/epg")
      .then(res => res.json())
      .then(result => {
        this.setState({
          isLoaded: true,
          channel_info: result
        });
      });
  }

  render() {
    const screenHeight = window.innerHeight;
    let prevTime = new Date(1482224400000);
    let currentTime = new Date(2016, 11, 20, 14, 30, 0, 0); //hard code as of now (Tue Dec 20 2016 14:30:00 GMT+0000 ) can be replace with current time
    let currentIndicatorMargin =
      ((currentTime - prevTime) * 5000) / (24 * 3600 * 1000)+4;
    const LoadedContent = !this.state.isLoaded ? (
      <h1 className="dataNotFound">Data is not availble!!</h1>
    ) : (
      <div id="timeline-wrapper" style={{ height: screenHeight - 180 }}>
        <div id="channel_details">
          {this.state.images.map((key) => {
            return (
              <div key={key} className="channel_logo">
                <img className="channel-image" src={key} />
              </div>
            );
          })}
        </div>
        <div id="show-timings-wrapper">
          <div className="show-inner-div">
            <TimeBar prevTime={prevTime}/>
          </div>
          <div className="show-inner-div">
            {this.state.channel_info.map((channel, key) => {
              prevTime = new Date(1482224400000);
              return (
                <div key={key}>
                  {channel.schedules.map((show, key) => {
                    const showLeftMargin =
                      ((show.start - prevTime) * 5000) / (24 * 3600 * 1000);
                    const showLength =
                      ((show.end - show.start) * 5000) / (24 * 3600 * 1000);
                    prevTime = show.end;
                    const backColor = (show.start <= currentTime && show.end >= currentTime) ? "#2f2e2e" : "black";
                    return (
                      <div
                        key={key}
                        className="show-details-div"
                        style={{
                          marginLeft: showLeftMargin,
                          width: showLength,
                          background : backColor
                        }}
                      >
                        <div className="show-desc">{show.title}</div>
                        <div className="show-times">
                          {new Date(show.start).getHours() +
                            "." +
                            new Date(show.start).getMinutes() +
                            " - " +
                            new Date(show.end).getHours() +
                            "." +
                            new Date(show.end).getMinutes()}
                        </div>
                      </div>
                    );
                  })}
                </div>
              );
            })}
            <div
              id="current-indicator"
              style={{ marginLeft: currentIndicatorMargin }}
            />
          </div>
        </div>
        <NowButton />
      </div>
    );

    return LoadedContent;
  }
}

export default TimeLine;
