import React, { Component } from 'react';
import style from './app.css';
import Header from './components/header/Header';
import Footer from './components/footer/Footer';
import DateBar from './components/DateBar/Datebar';
import TimeLine from './components/ShowTimeLine/Timeline';


class App extends Component{
   render(){
      return(
        <div className="container">
        <Header />
        <DateBar />
        <TimeLine />
        <Footer />
      </div>
      );
   }
}
export default App;