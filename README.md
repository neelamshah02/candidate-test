## Project Execution Method
To run this project do below:
 1.start the mock server
   * clone the repo https://github.com/NoriginMedia/mock-api.git.
   * npm run start:demo
 2.start the development server.
   * npm install
   * npm start

##image of the working model
image : https://photos.google.com/photo/AF1QipPXYAQ3rx9zfV2KGg0PzDFx5MLvEnmEDEESc2F2

##functionality
   * data retrieved from the mocked api is displayed on the timeline with horizontal scroll enabled.
   * now button is will scroll the screen to the current time assumed(2:30).
   * It is assumed the current date as 20th December, 2016 as per API returned data.
   * Header and Footer displaying icons is as per design and responsive in nature.
   * there is a date bar that is scrollable and displays one week of future dates.
 
##Contact:
   * For any queries please contact on below email
   * email: neelamshah021189@gmail.com


